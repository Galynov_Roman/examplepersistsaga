export const FETCHING_DATA = 'FETCHING_DATA';
export const CANCEL_FETCHING_DATA = 'CANCEL_FETCHING_DATA';
export const FETCHING_DATA_SUCCESS = 'FETCHING_DATA_SUCCESS';
export const FETCHING_DATA_FAILURE = 'FETCHING_DATA_FAILURE';

export const fetchData = () => ({
  type: FETCHING_DATA,
});

export const cancelFetchData = () => ({
  type: CANCEL_FETCHING_DATA,
});

export const getDataSuccess = data => ({
  type: FETCHING_DATA_SUCCESS,
  data,
});

export const getDataFailure = error => ({
  type: FETCHING_DATA_FAILURE,
  errorMessage: error,
});
