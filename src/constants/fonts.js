export const fonts = {
  regular: 'SFUIDisplay-Regular',
  bold: 'SFUIDisplay-Semibold',
  medium: 'SFUIDisplay-Medium',
  light: 'SFUIDisplay-Light',
  textBold: 'SFUIText-Bold',
  textLight: 'SFUIText-Light',
  textMedium: 'SFUIText-Medium',
};
