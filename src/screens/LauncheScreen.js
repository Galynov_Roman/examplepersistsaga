import React from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  TouchableHighlight,
  Button,
} from 'react-native';

import { connect } from 'react-redux';
import { fetchData, cancelFetchData } from '../actions/data';
import { colors } from '../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  mainContent: {
    flex: 1,
  },
});

class LauncheScreen extends React.Component {
  static propTypes = {
  };
  static defaultProps = {
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    const {
      data,
      fetchData,
      cancelFetchData,
      navigation,
    } = this.props;
    // console.warn('>>>', data);
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <Button
          onPress={() => navigation.navigate('SecondScreen')}
          title="Go to Second Screen"
        />
        <View style={styles.container}>
          <Text>Redux-Saga Example</Text>
          <TouchableHighlight onPress={() => fetchData()}>
            <Text>Load Data</Text>
          </TouchableHighlight>
          <View style={styles.mainContent}>
            {
              data.data.length ? (
                data.data.map((person, i) => (
                  <View key={i} >
                    <Text>Name: {person.name}</Text>
                    <Text>Age: {person.age}</Text>
                  </View>))
              ) : null
            }
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.data,
});

const mapDispatchToProps = dispatch => ({
  fetchData: () => dispatch(fetchData()),
  cancelFetchData: () => dispatch(cancelFetchData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LauncheScreen);
