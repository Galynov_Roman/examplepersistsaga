import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import { connect } from 'react-redux';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

class SecondScreen extends React.Component {
  static propTypes = {
  };
  static defaultProps = {
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Second Screen</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  // auth: state.auth.authData,
  // networkIsConnected: state.network.networkIsConnected,
  // infoPanel: state.infoPanel.infoPanelData,
});

const mapDispatchToProps = dispatch => ({
  // getData: () => dispatch(DashboardActions.requestDataChipsFree()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SecondScreen);
